-- load config via lua-load
-- defaults below
if not acme_config then
	acme_config = {
		env_var = "ACME_ACCOUNT_THUMBPRINT",
		hap_var = "txn.acme_account_thumbprint",
		thumbprint = nil
	}
end

core.register_init(function()
	-- if thumbprint is not set via config then get from environment
	if acme_config.thumbprint == nil then
		acme_config.thumbprint = os.getenv(acme_config.env_var)
	end
end)

core.register_service("acme-stateless", "http", function(applet)
	local p = applet.path
	local response
	local c = core.concat()
	local gsub = string.gsub
	local len = string.len
	local status = 200
	local thumbprint
	-- if thumbprint is not set then load from haproxy variable
	if acme_config.thumbprint == nil then
		thumbprint = applet.get_var(applet, acme_config.hap_var)
		-- if variable not found then return a 404
		if thumbprint == nil then
			status = 404
		end
	else
		thumbprint = acme_config.thumbprint
	end
	response, _ = gsub(p, "/.well%-known/acme%-challenge/", "")
	c:add(response)
	c:add(".")
	c:add(thumbprint)
	response = c:dump()
	applet:set_status(status)
	applet:add_header("Content-Length", len(response))
	applet:add_header("Content-Type", "text/plain")
	applet:start_response()
	applet:send(response)
end)
