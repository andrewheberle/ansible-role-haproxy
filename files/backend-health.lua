-- load config via lua-load
-- defaults below
if not backend_health_config then
	backend_health_config = {
		path_prefix = nil,
		env_var = "BACKEND_HEALTH_PATH_PREFIX"
	}
end

function string:split(sep)
	local sep, fields = sep or ":", {}
	local pattern = string.format("([^%s]+)", sep)
	self:gsub(pattern, function(c) fields[#fields+1] = c end)
	return fields
end

core.register_init(function()
	-- if path_prefix is not set via config then get from environment
	if backend_health_config.path_prefix == nil then
		backend_health_config.path_prefix = os.getenv(backend_health_config.env_var)
		-- if not set via environment then use default
		if backend_health_config.path_prefix == nil then
			backend_health_config.path_prefix = "healthcheck"
		end
	end
end)

core.register_service("backend-health", "http", function(applet)
	local gsub = string.gsub
	local len = string.len
	local printf = string.format
	local p = gsub(applet.path, "/", "", 1)
	local response
	local status = 200
	local spath = p:split("/")
	local backends = core.backends
	local be
	local stats

	if spath[1] == nil or spath[2] == nil or spath[3] == nil then
		status = 400
		response = '{"status": "error", "message": "invalid request"}'
	else
		if spath[2] == "backend" then
			be = backends[spath[3]]
			if be ~= nil then
				stats = be:get_stats()
				response = printf('{"status": "%s"}', stats.status)
			else
				status = 404
				response = '{"status": "error", "message": "not found"}'
			end
		else
			status = 404
			response = '{"status": "error", "message": "not found"}'
		end
	end
	applet:set_status(status)
	applet:add_header("Content-Length", len(response))
	applet:add_header("Content-Type", "application/json")
	applet:start_response()
	applet:send(response)
end)
